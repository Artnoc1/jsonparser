const mongoose = require('mongoose')
const Schema = mongoose.Schema

const JSSchema = new Schema ({
    ID:{
        type: String,
    },
    UUID:{
        type:String
    },
    type:{
        type:String
    }

})

module.exports = mongoose.model('JSSchema', JSSchema)
