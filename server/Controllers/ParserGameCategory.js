const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MjQ0NzI0LCJleHAiOjE1OTQyNDgzMjR9.9TNGY19yIrnGUbXXlnza0daamUlF1_ccse77uXIbJ6k";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "6dc2a2137d814c29a897f735126af470"
const CONTENTTYPE = "gamecategory"
const CONTENTTYPEJSON = "gameCategory"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let linkObj = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id


            //Relation IMAGES
            if (Demon.fields.image) {
                if (Demon.fields.image['en-GB'].sys.id) {
                    thumbObj = await JSSchema.findOne({
                        ID: Demon.fields.image['en-GB'].sys.id,
                        type: 'images'
                    })
                } else {
                    thumbObj = null
                }
            }
            //Realation Link
            if (Demon.fields.action) {
                if (Demon.fields.action['en-GB'].sys.id) {
                    linkObj = await JSSchema.findOne({
                        ID: Demon.fields.action['en-GB'].sys.id,
                        type: 'linkitem'
                    })
                } else {
                    linkObj = null
                }
            }

            console.log("linkObj", linkObj)

            jsonConstructor(Demon, thumbObj, linkObj).then(result => {
                console.log("RESULT", result)
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })
                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, thumbObj, linkObj) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject
            let str
            let linkObjvalue
            //    get Demon Key's
            if (linkObj) {
                linkObjvalue = {uuid : linkObj }
            }else {
                linkObjvalue = null
            }


            if (thumbObj) {
                newObject = new Object({id: Demon.sys.id, image: {uuid: thumbObj.UUID} });
                if (linkObj) {
                    newObject = new Object({id: Demon.sys.id, image: {uuid: thumbObj.UUID}, action : {uuid: linkObj.UUID} });
                }
            } else {
                newObject = new Object({id: Demon.sys.id});
                if (linkObj) {
                    newObject = new Object({id: Demon.sys.id, action : {uuid: linkObj.UUID} });
                }
            }

            for (let DemonKey in Demon.fields) {
                if (DemonKey !== 'image') {
                    if (DemonKey !== 'games') {
                        stringValue = `${DemonKey}.en-GB`
                        str = tr[stringValue]
                        newObject[DemonKey.toLowerCase()] = str
                    }
                }
            }
            newFieldData.push(newObject)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
