const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')

const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MjM5MTMyLCJleHAiOjE1OTQyNDI3MzJ9.6a_FWdR82PA6WjmlvWlo5P82XsWATs-gQvnInUgBm2s";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentJSON = require('../../assets.json')
const contentJSONArr = contentJSON.assets

module.exports.dataParser = async (req, res) => {
    try {
        const key = req.params.key
        const ID = contentJSONArr[key].sys.id
        const data = contentJSONArr[key]
        const fields = contentJSONArr[key].fields
        const obj = await JSSchema.findOne({
            ID: ID,
            type: 'images'
        })
        let imagesUuid
        // Get Local Image

        // IF
        if (!obj) {
            imagesFirstStep(fields, ID).then(result => {
                return result
            }).then(result => {
                imagesSecondStep(result, ID).then(result => {
                    return result
                }).then(result => {
                    createMongoose(result, ID).then(result => {
                        res.status(200).json({
                            success: true,
                            result
                        })
                    })
                })
            })
        }


    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions
const imagesFirstStep = async (fields, ID) => {
    return new Promise((resolve, reject) => {

        let title = ''
        let description = ''
        if (fields.title) {
            title = fields.title['en-GB'] ? fields.title['en-GB'] : ''
        }
        if (fields.description) {
            description = fields.description['en-GB'] ? fields.description['en-GB'] : ''
        }
        let imgBody = {
            "parentNodeUuid": "5007d5a707ad48969b3754b0dac71682",
            "language": "en",
            "schema": {"name": "image"},
            "fields": {
                "id": ID,
                "title": title,
                "description": description
            }
        }
        // // Fetch
        fetch(BASE, {
            method: 'post',
            body: JSON.stringify(imgBody),
            headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
        })
            .then(response => response.json())
            .then(json => {
                if (json.uuid) {
                    resolve(json.uuid)
                } else {
                    resolve('Error')
                }
            })
    })
}

const imagesSecondStep = async (result, ID) => {
    return new Promise((resolve, reject) => {
        // Get Local Image
        let directoryPath
        let directoryPathFolder
        let fileName;
        let imgURL;
        directoryPath = path.join(__dirname, 'images/' + ID);
        fs.readdir(directoryPath, function (err, files) {
            if (files) {
                directoryPathFolder = files[0];
                fs.readdir(directoryPath + '/' + directoryPathFolder + '/', function (err, files) {
                    fileName = files[0]
                    imgURL = directoryPath + '/' + directoryPathFolder + '/' + fileName

                });
            }
        });
        setTimeout(() => {
            const formData = new FormData
            const fileField = fs.createReadStream(`${imgURL}`);
            formData.append('file', fileField);
            formData.append('language', 'en');
            formData.append('version', '0.1');

            fetch(`https://gentics-mesh.smartsource.ge/api/v1/casino999a/nodes/${result}/binary/file?lang=en`, {
                method: 'post',
                body: formData,
                headers: {'Authorization': TOKEN},
            })
                .then(response => response.json())
                .then(json => {
                    if (json) {
                        //console.log("json", json)
                        resolve(json)
                    }
                });
        }, 1000)
    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        console.log("result", result.uuid)
        console.log("ID", ID)

        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: 'images'
            }).save()
            resolve(NewSchem)
        }else {
            resolve('ERROR!!!!!')
        }
    })
}
