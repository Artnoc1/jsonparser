const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')

const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MzA2NzQyLCJleHAiOjE1OTQzMTAzNDJ9.nMZunFabkvLaPvd0eckYGQiCQrNHGUmBKk0L6YoJVHc";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "23cdc2ff4c9d408aac2b1ed954cb70bf"
const CONTENTTYPE = "page"
const CONTENTTYPEJSON = "page"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let thumbObj2 = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id


            //Relation IMAGES
            if (Demon.fields.background) {
                if (Demon.fields.background['en-GB'].sys.id) {
                    thumbObj = await JSSchema.findOne({
                        ID: Demon.fields.background['en-GB'].sys.id,
                        type: 'imageitem'
                    })
                } else {
                    thumbObj = null
                }
            }
            //Relation IMAGES
            if (Demon.fields.logo) {
                if (Demon.fields.logo['en-GB'].sys.id) {
                    thumbObj2 = await JSSchema.findOne({
                        ID: Demon.fields.logo['en-GB'].sys.id,
                        type: 'imageitem'
                    })
                } else {
                    thumbObj2 = null
                }
            }


            //Realation Link ARRAY
            let linkObj = '';
            if (Demon.fields.action) {
                if (Demon.fields.action['en-GB'].sys.id) {
                    linkObj = await JSSchema.findOne({
                        ID: Demon.fields.action['en-GB'].sys.id,
                        type: 'linkitem'
                    })
                } else {
                    linkObj = null
                }
            }


            jsonConstructor(Demon, thumbObj, thumbObj2, linkObj).then(result => {
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    console.log("RE",result)
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })

                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, thumbObj, thumbObj2, linkObj) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject = new Object()
            let str
            let linkObjvalue
            //    get Demon Key's

            // console.log("arrayObject", arrayObject)
            newObject = new Object({
                background: thumbObj ? {uuid: thumbObj.UUID} : null,
                logo :  thumbObj2 ? {uuid: thumbObj2.UUID} : null
            });

            for (let DemonKey in Demon.fields) {
                if (DemonKey !== "background") {
                    if (DemonKey !== "logo") {
                        if (DemonKey !== "action") {
                            stringValue = `${DemonKey}.en-GB`
                            str = tr[stringValue]
                            newObject[DemonKey.toLowerCase()] = str
                        }
                    }
                }
            }
            newFieldData.push(newObject)
            console.log("newObject", newObject)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
