const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MjkxMjA0LCJleHAiOjE1OTQyOTQ4MDR9.PDB-_C6-3s_jR8k4hpi4sEDOFMQFcdxZY95OwkeMtWI";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "cb676ae229a84f0d98f30ab9427859b6"
const CONTENTTYPE = "infopage"
const CONTENTTYPEJSON = "infoPage"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let thumbObj2 = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id


            // console.log("!!!!!!!",Demon)
            //Realation Link ARRAY
            let linkObjN =  await JSSchema.find({
                type: 'documentsection'
            })
            const fuse = new Fuse(linkObjN, {
                keys: [
                    'ID'
                ]
            })
            //
            let objList = {}
            let fieldList = []
            let arrayObject
            if (Demon.fields.sections) {
                Demon.fields.sections['en-GB'].map((item) => {
                    let results = fuse.search(item.sys.id)
                    if (results.length>0) {
                        objList = {uuid: results[0].item.UUID}
                        fieldList.push(objList)
                    }
                })
                arrayObject = new Object( {seals : fieldList})
            }

            //Realation Link
            let linkObj;
            if (Demon.fields.pageMeta) {

                if (Demon.fields.pageMeta['en-GB'].sys.id) {
                    linkObj = await JSSchema.findOne({
                        ID: Demon.fields.pageMeta['en-GB'].sys.id,
                        type: 'pagemeta'
                    })
                } else {
                    linkObj = null
                }
            }

            jsonConstructor(Demon, arrayObject, linkObj).then(result => {
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    console.log("RE",result)
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })
                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, arrayObject, linkObj) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject = new Object()
            let str
            let linkObjvalue
            //    get Demon Key's

            newObject = new Object({
                pagemeta :  linkObj ? {uuid: linkObj.UUID} : null,
                sections: arrayObject ? arrayObject.seals : null
            });


            for (let DemonKey in Demon.fields) {

                if (DemonKey !== 'sections') {
                    if (DemonKey !== 'pageMeta') {
                        stringValue = `${DemonKey}.en-GB`
                        str = tr[stringValue]
                        newObject[DemonKey.toLowerCase()] = str
                    }
                }
            }
            newFieldData.push(newObject)
            //console.log("!!!!!!!!!!!!", newFieldData)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
