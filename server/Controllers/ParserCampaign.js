const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0Mjg3ODU0LCJleHAiOjE1OTQyOTE0NTR9.okAkpwNmSjNsTC6q7QyiUT5G8l1E81AWRhkxw4irBNQ";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "270f9dccb1214820a94253187c705ecb"
const CONTENTTYPE = "campaign"
const CONTENTTYPEJSON = "campaign"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let thumbObj2 = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id

           // console.log("Demon", Demon.fields.action['en-GB'].sys.id)
            //Relation IMAGES
            if (Demon.fields.thumb) {
                if (Demon.fields.thumb['en-GB'].sys.id) {
                    thumbObj = await JSSchema.findOne({
                        ID: Demon.fields.thumb['en-GB'].sys.id,
                        type: 'images'
                    })
                   // console.log("thumbObj", thumbObj)
                } else {
                    thumbObj = null
                }
            }
            //Relation IMAGES
           // console.log("thumbObj", thumbObj)

            //Realation Link ARRAY
            let linkObj = '';
            if (Demon.fields.action) {
                if (Demon.fields.action['en-GB'].sys.id) {
                    linkObj = await JSSchema.findOne({
                        ID: Demon.fields.action['en-GB'].sys.id,
                        type: 'linkitem'
                    })
                } else {
                    linkObj = null
                }
            }

            jsonConstructor(Demon, thumbObj, linkObj).then(result => {
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    console.log("RE",result)
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })

                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, thumbObj, linkObj) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject = new Object()
            let str
            let linkObjvalue
            //    get Demon Key's

            console.log("!", linkObj)
            // console.log("arrayObject", arrayObject)
            newObject = new Object({
                thumb :  thumbObj ? {uuid: thumbObj.UUID} : null,
                action : linkObj ? linkObj.seals : null
            });

            for (let DemonKey in Demon.fields) {
               if (DemonKey !== 'action') {
                   if (DemonKey !== 'thumb') {
                       stringValue = `${DemonKey}.en-GB`
                       str = tr[stringValue]
                       newObject[DemonKey.toLowerCase()] = str
                   }

               }
            }
            newFieldData.push(newObject)

             resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
