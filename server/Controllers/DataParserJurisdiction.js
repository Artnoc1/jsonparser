const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MjM5NTI5LCJleHAiOjE1OTQyNDMxMjl9.lCj-SKk1cCOgJZk7_BTseZTR70zhcEY8u9cKNCxWA8U";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "69fe519860094831963188d9e1b202ab"
const CONTENTTYPE = "jurisdiction"
const CONTENTTYPEJSON = "jurisdiction"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let linkObj = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id


            //Realation Link Array
            linkObj =  await JSSchema.find({
                type: 'linkimage'
            })
            const fuse = new Fuse(linkObj, {
                keys: [
                    'ID'
                ]
            })
            //
            let objList = {}
            let fieldList = []
            let arrayObject
            if (Demon.fields.seals) {
                    Demon.fields.seals['en-GB'].map((item) => {
                        let results = fuse.search(item.sys.id)
                        objList = {uuid: results[0].item.UUID}
                        fieldList.push(objList)
                        // console.log("!!!", results)
                    })
                arrayObject = new Object( {seals : fieldList})
            }

            jsonConstructor(Demon, arrayObject).then(result => {
               // console.log("RESULT", result)
                return result
            }).then(result => {
                fetchData(result).then(json => {

                    console.log("arrayObject", result)
                    return json
                }).then(result => {
                    // createMongoose(result, ID).then(finish => {
                    //     console.log("FINISH", finish)
                    // })
                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, arrayObject) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject
            let str
            let linkObjvalue
            //    get Demon Key's


            st = new Object({id: Demon.sys.id, seals : arrayObject.seals });
            for (let DemonKey in Demon.fields) {
                if (DemonKey !== 'seals') {
                    stringValue = `${DemonKey}.en-GB`
                    str = tr[stringValue]
                    st[DemonKey.toLowerCase()] = str
                }
            }
            newFieldData.push(st)
            //console.log("!!!!!!!!!!!!!!!!!!!!!!!!!", newFieldData)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
