const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MjgzMDM0LCJleHAiOjE1OTQyODY2MzR9.bQpvkzMnjURMJNzz_CMVMvVJ67OuhiKEMw8U0M9I3Uo";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "e51d21b29ecc4785b9193c30abf26f2c"
const CONTENTTYPE = "banner"
const CONTENTTYPEJSON = "banner"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let thumbObj2 = '';
            let thumbObj3 = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id


            //Realation Link ARRAY
            let linkObj = '';
            linkObj =  await JSSchema.find({
                type: 'banneritem'
            })
            const fuse = new Fuse(linkObj, {
                keys: [
                    'ID'
                ]
            })
            //
            let objList = {}
            let fieldList = []
            let arrayObject
            if (Demon.fields.items) {
                Demon.fields.items['en-GB'].map((item) => {
                   // console.log("!!")
                    let results = fuse.search(item.sys.id)
                    if (results.length>0) {
                        objList = {uuid: results[0].item.UUID}
                        fieldList.push(objList)
                    }

                })
                arrayObject = new Object( {items : fieldList})
            }


            jsonConstructor(Demon, arrayObject).then(result => {
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    console.log("RE",result)
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })
                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, arrayObject) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject = new Object()
            let str
            let linkObjvalue
            //    get Demon Key's
          // console.log("arrayObject", arrayObject)
            if (arrayObject) {
                newObject = new Object({id: Demon.sys.id,
                    items : arrayObject ? arrayObject.items : null
                });
            }
            for (let DemonKey in Demon.fields) {
                if (DemonKey !== "items") {
                    stringValue = `${DemonKey}.en-GB`
                    str = tr[stringValue]
                    newObject[DemonKey.toLowerCase()] = str
                }
            }
            newFieldData.push(newObject)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
