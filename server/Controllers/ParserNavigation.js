const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MzAyMzgwLCJleHAiOjE1OTQzMDU5ODB9.9B35fenVh51IfsEwBe7Lduipk-jocc_z8YrYPWZppko";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "6349923558324f6caed8fe606ffd4e4b"
const CONTENTTYPE = "navigation"
const CONTENTTYPEJSON = "navigation"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            const Demon = fieldData[key];
            const ID = Demon.sys.id


            //Realation Link ARRAY
            let linkObj = '';
            linkObj = await JSSchema.find({
                type: 'linkitem'
            })
            const fuse = new Fuse(linkObj, {
                keys: [
                    'ID'
                ]
            })
            //
            let objList = {}
            let fieldList = []
            let arrayObject
            if (Demon.fields.links) {
                Demon.fields.links['en-GB'].map((item) => {
                    let results = fuse.search(item.sys.id)
                    if (results.length > 0) {
                        objList = {uuid: results[0].item.UUID}
                        fieldList.push(objList)
                    }

                })
                arrayObject = new Object({links: fieldList})
            }

            jsonConstructor(Demon, arrayObject).then(result => {
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    console.log("RE",result)
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })
                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, arrayObject) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject = new Object()
            let str
            let linkObjvalue
            //    get Demon Key's
            // console.log("arrayObject", arrayObject)
            newObject = new Object({
                links: arrayObject ? arrayObject.links : null
            });
            for (let DemonKey in Demon.fields) {
                if (DemonKey !== "children") {
                    if (DemonKey !== "links") {
                        stringValue = `${DemonKey}.en-GB`
                        str = tr[stringValue]
                        newObject[DemonKey.toLowerCase()] = str
                    }
                }
            }
            newFieldData.push(newObject)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}

