const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const Fuse = require('fuse.js')

const JSSchema = require('../Model/JSSchema')


const fetchTurn = 1
const TOKEN = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyVXVpZCI6ImUxNTJkZmM0ZjVkYzQyODg4N2E5MDk0N2NlZDZjMmE5IiwiaWF0IjoxNTk0MjQ1OTIxLCJleHAiOjE1OTQyNDk1MjF9.wFUuN8IqVS_X_Grl1jmcdLNb1lx8OWuM52CmKRkVkVg";
const BASE = "https://gentics-mesh.smartsource.ge/api/v2/casino999a/nodes"

// JSON
const contentEntry = require('../../entry.json')
const contentEntryArray = contentEntry.entries

const PARENTNODEUUID = "263fab62c0c04bb4a2c36daefafa6f4c"
const CONTENTTYPE = "game"
const CONTENTTYPEJSON = "game"

module.exports.dataParser = async (req, res) => {
    try {
        let key = req.params.key
        //console.log("contentJSONArr[0];", contentJSONArr[key])
        if (key) {
            let a = 0
            let fieldData = []
            let objField = {}
            contentEntryArray.map((item) => {
                if (item.sys.contentType.sys.id === CONTENTTYPEJSON) {
                    objField = item
                    fieldData.push(objField)
                    a++;
                }
            });

            let thumbObj = '';
            let thumbObj2 = '';
            const Demon = fieldData[key];
            const ID = Demon.sys.id


            console.log("!!!!!!!",Demon)
            //Relation IMAGES
            if (Demon.fields.gameThumb) {
                if (Demon.fields.gameThumb['en-GB'].sys.id) {
                    thumbObj = await JSSchema.findOne({
                        ID: Demon.fields.gameThumb['en-GB'].sys.id,
                        type: 'images'
                    })
                } else {
                    thumbObj = null
                }
            }
                        //Relation IMAGES
            if (Demon.fields.gameBackground) {
                if (Demon.fields.gameBackground['en-GB'].sys.id) {
                    thumbObj2 = await JSSchema.findOne({
                        ID: Demon.fields.gameBackground['en-GB'].sys.id,
                        type: 'images'
                    })
                } else {
                    thumbObj2 = null
                }
            }


          console.log("thumbObj", thumbObj)
          console.log("thumbObj2", thumbObj2)

            //Realation Link ARRAY
            let linkObj = '';
            linkObj =  await JSSchema.find({
                type: 'gamecategory'
            })
            const fuse = new Fuse(linkObj, {
                keys: [
                    'ID'
                ]
            })
            //
            let objList = {}
            let fieldList = []
            let arrayObject
            if (Demon.fields.categories) {
                Demon.fields.categories['en-GB'].map((item) => {
                    let results = fuse.search(item.sys.id)
                    if (results.length>0) {
                        objList = {uuid: results[0].item.UUID}
                        fieldList.push(objList)
                    }

                })
                arrayObject = new Object( {seals : fieldList})
            }


            jsonConstructor(Demon, thumbObj, thumbObj2, arrayObject).then(result => {
                return result
            }).then(result => {
                fetchData(result).then(json => {
                    return json
                }).then(result => {
                    console.log("RE",result)
                    createMongoose(result, ID).then(finish => {
                        console.log("FINISH", finish)
                    })

                })
            })

        }
        res.status(200).json({
            success: "it's Ok",
        })

    } catch (e) {
        console.log("ERROR!", e)
    }
}


// Functions

const jsonConstructor = async (Demon, thumbObj, thumbObj2, arrayObject) => {
    return new Promise((resolve, reject) => {
        if (Demon) {
            let string;
            let stringValue;
            let newFieldData = []
            let tr = flatten(Demon.fields)
            let newObject
            let str
            let linkObjvalue
            //    get Demon Key's

       // console.log("arrayObject", arrayObject)
            if (thumbObj) {
                newObject = new Object({id: Demon.sys.id,
                    gamethumb: {uuid: thumbObj.UUID},
                    gamebackground :  thumbObj2 ? {uuid: thumbObj2.UUID} : null,
                    categories : arrayObject ? arrayObject.seals : null
                });
            }

            for (let DemonKey in Demon.fields) {
                if (DemonKey !== "gameThumb") {
                    if (DemonKey !== "gameBackground") {
                        if (DemonKey !== "categories") {
                            stringValue = `${DemonKey}.en-GB`
                            str = tr[stringValue]
                            newObject[DemonKey.toLowerCase()] = str
                        }
                    }
                }
            }
            newFieldData.push(newObject)
            resolve(newFieldData);

        }

    })
}


const fetchData = async (fields) => {
    return new Promise((resolve, reject) => {
        let Body = {
            "parentNodeUuid": PARENTNODEUUID,
            "language": "en",
            "schema": {"name": CONTENTTYPE},
            "fields": fields[0]
        }
        //
        if (fetchTurn === 1) {
            fetch(BASE, {
                method: 'post',
                body: JSON.stringify(Body),
                headers: {'Content-Type': 'application/json', 'Authorization': TOKEN},
            })
                .then(res => res.json())
                .then(json => {
                    resolve(json)
                });
        }

    })
}
const createMongoose = async (result, ID) => {
    return new Promise((resolve, reject) => {
        if (result.uuid) {
            const NewSchem = new JSSchema({
                ID: ID,
                UUID: result.uuid,
                type: CONTENTTYPE
            }).save()
            resolve(NewSchem)
        } else {
            resolve('ERROR!!!!!')
        }
    })
}
