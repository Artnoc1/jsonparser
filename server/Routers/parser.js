const express = require('express'); 
const controllerData = require('../Controllers/dataParser');
const controllerdataImageParser = require('../Controllers/dataImageParser');

const router = express.Router();

router.get('/image/:key', controllerdataImageParser.dataParser);
router.get('/data/:key', controllerData.dataParser);

module.exports = router;
