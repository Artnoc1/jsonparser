const express = require('express')
const fetch = require('node-fetch');
const path = require('path')
const fs = require('fs')
const flatten = require('flat')
const FormData = require('form-data');
const mongoose = require('mongoose')
// Constants
const app = express()
const ParserTurn = true;

//Mongose
mongoose.connect('mongodb://localhost:27017/casino999', {useNewUrlParser:true})
    .then(() => console.log('MongoDB connection success'))
    .catch(error => console.log(error))

// JSON Assets
const contentJSON = require('../assets.json')
const contentJSONArr = contentJSON.assets

// DB Parser
const contentEntry = require('../entry.json')
const contentEntryArray = contentEntry.entries

let a = 0
let fieldData = []
let objField = {}
contentEntryArray.map((item) => {
    // console.log(`#${a} - ${item.sys.id}`)
    if (item.sys.contentType.sys.id === "translations") {
        objField = item.fields
        fieldData.push(objField)
        console.log("A", a)
        a++;
    }
});


console.log("contentJSONArr", contentJSONArr.length)

function parserMove() {
    let dataLength = 1;
    let id = setInterval(frame, 1500);
    function frame() {
        if (dataLength === 953) { //contentJSONArr.length 27
            clearInterval(id);
            console.log("Parsing is finished")
        } else {
            fetch(`http://localhost:3100/api/parser/data/${dataLength}`, {
                method: 'GET',
                headers: { 'Content-Type': 'application/json'  },
            })
                .then(res => res.json())
                .then(json => {
                    //console.log(json)
                });
            console.log(dataLength)
            dataLength++;
        }
    }
}

/*
// Images
function parserMove() {
    let dataLength = 2195;
    let id = setInterval(frame, 1500);
    function frame() {
        if (dataLength === 2264) { //contentJSONArr.length
            clearInterval(id);
            console.log("Parsing is finished")
        } else {
            fetch(`http://localhost:3101/api/parser/image/${dataLength}`, {
                method: 'GET',
                headers: { 'Content-Type': 'application/json'  },
            })
                .then(res => res.json())
                .then(json => {
                    //console.log(json)
                });
            console.log(dataLength)
            dataLength++;
        }
    }
}
*/

if (ParserTurn) {
  // parserMove();
}

const Demon = fieldData[0];


// Router
const parser = require('./Routers/parser.js')
app.use('/api/parser', parser);

app.get('/eventparser', (req, res) => {
    res.send(Demon)
})

app.get('/', (req, res) => res.send('Hello, i am sexsy JSON parser !'))
const port = 3100
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))
